from receipts.models import Receipt, ExpenseCategory, Account
from django import forms


class CreateReceiptForm(forms.ModelForm):
    class Meta:
        model = Receipt
        fields = ['vendor', 'total', 'tax', 'date', 'category', 'account']


class CreateExpenseCategoryForm(forms.ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ['name']


class CreateAccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = ['name', 'number']
