from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import CreateReceiptForm, CreateExpenseCategoryForm, CreateAccountForm
from django.contrib.auth.decorators import login_required
# Create your views here.


@login_required
def show_receipts(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {
        'receipt_list': receipt_list
    }
    return render(request, 'receipts/list.html', context)


@login_required
def create_receipt(request):
    if request.method == 'POST':
        form = CreateReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')

    else:
        form = CreateReceiptForm()

    context = {
        "form": form
    }

    return render(request, 'receipts/create.html', context)


@login_required
def create_category(request):
    if request.method == 'POST':
        form = CreateExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect('show_categories')
    else:
        form = CreateExpenseCategoryForm()

    context = {
        "form": form
    }

    return render(request, 'categories/create.html', context)

@login_required
def create_account(request):
    if request.method == 'POST':
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect('show_accounts')
    else:
        form = CreateAccountForm()

    context = {
        "form": form
    }

    return render(request, 'accounts/create.html', context)
@login_required
def show_categories(request):
    category_list = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        'category_list': category_list
    }

    return render(request, 'receipts/categories.html', context)


@login_required
def show_accounts(request):
    account_list = Account.objects.filter(owner=request.user)
    context = {
        'account_list': account_list
    }

    return render(request, 'receipts/accounts.html', context)
