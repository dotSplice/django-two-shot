from django.urls import path
from receipts.views import show_receipts, create_receipt, show_categories, show_accounts, create_category, create_account

urlpatterns = [
    path('', show_receipts, name='home'),
    path('create/', create_receipt, name='create_receipt'),
    path('categories/', show_categories, name='show_categories'),
    path('categories/create/', create_category, name='create_category'),
    path('accounts/', show_accounts, name='show_accounts'),
    path('accounts/create/', create_account, name='create_account'),
]
